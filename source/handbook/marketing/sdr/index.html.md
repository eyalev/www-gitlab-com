---
layout: markdown_page
title: "Sales Development"
---

---
## On this page
{:.no_toc}

- TOC
{:toc}

---

# Inbound SDR Handbook


## [Welcome](#welcome)
- [Your Role](#role)
- [Onboarding](#onboard)
- [BDR & Team Lead](#teamLead)

## [BDR Workflow](#bdring)
- [Lead/MQL Definition](#mql)
    - [A Leads](#aLeads)
    - [Passive Leads](#passiveLeads)
- [Lead Management](#leadManagement)
    - [Glossary](#glossary)
    - [Segmentation](#segmentation)
    - [Lead Routing](#leadRouting)
    - [Lead Source](#leadSource)
    - [Lead Status](#leadStatus)
- [Researching](#researching)
- [Prospecting](#prospecting)
   - [Email Prospecting](#emailProspect)
   - [Call Prospecting](#callProspect)
- [Qualifying](#qualifying)
   - [SQL Qualification Criteria](#sql)
   - [When to Create an Opportunity](#opportunity)

## [Miscellaneous](#misc)
- [Variable Compensation Guidelines](#compensation)
- [Additional Resources](#additionalResources)
- [FAQ](#questions)

# Welcome<a name="welcome"></a>

Welcome to GitLab and congratulations on landing a job with the best open source tech company! We are excited to have you join the team and look forward to working closely with you and seeing you grow and develop into a top performing salesperson.

As a BDR (Business Development Representative) your focus will be on qualifying inbound leads. On this team we work hard, but have fun too. We will work hard to guarantee your success if you do the same. We value results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, solutions, and quirkiness.

Being a BDR can come with what seems like long days, hard work, frustration, and even at times, failure. If you have the right mindset and come to work with a tenacious, hungry attitude you will see growth personally and in your career. GitLab is a place with endless opportunity. Let’s make it happen! 	

### Your Role<a name="role"></a>
As a BDR, you will be dealing with the front end of the sales process. You play a crucial role that helps bridge the gap between sales and marketing. You will be tasked with generating qualified opportunities for GitLab as you look for leads, research companies, industries, and different roles.

As you gain knowledge, you will be able to aid these key players in solving problems within the developer lifecycle. There are numerous resources at your fingertips that we have created to help you in this process. You have:

1. [BDR Handbook](https://about.gitlab.com/handbook/marketing/business-development/) - This will help you with your day to day workflow. You can find information on how to prospect, best practices, customer FAQs, buyer types, cadence samples and more. Use the BDR handbook in conjunction with the [Marketing](https://about.gitlab.com/handbook/marketing/) and [Sales](https://about.gitlab.com/handbook/sales/) handbooks. This will help you bridge the gap between the two and learn the product and process faster.
1. [GLU GitLab University](https://docs.gitlab.com/ce/university/) - These trainings will teach the fundamentals of Version Control with Git and GitLab through courses that cover topics which can be mastered in about 2 hours. These trainings include an intro to Git, GitLab basics, a demo of Gitlab.com, terminology, and more. You can also find information about GitLab compared to other tools in the market.
2. Gmail
3. Salesforce
4. [Outreach](https://outreach.io/)
4. LinkedIn
5. [Lattice](https://latticehq.com/) - A tool that tracks our OKRs (Objective and Key Results)
6. [Grovo](https://www.grovo.com/) - A learning management system with short, engaging tracks focused on specific skills.
7. Slack
8. Google Drive

### Onboarding<a name="onboard"></a>
You will be assigned an onboarding issue by Peopleops. Tasks in the issue will fill up the majority of your first week. This is a step by step guide/checklist to getting everything in your arsenal set up, such as equipment, tools, security, and your Gitlab.com account. These todo’s provide you with the fundamentals.

[BDR Weekly Onboarding Schedule](https://docs.google.com/a/gitlab.com/document/d/1yHZ0-oZAuq8keAZvFdeXSHdGC_bn3Fdr2VS2EO_Ug64/edit?usp=sharing) - This 3 week schedule will get you up to speed with the basics of BDRing.

### BDR & BDR Team Lead<a name="teamLead"></a>
#### Formal Weekly 1:1
- Mental check-in (winning and success)
- Discuss progress on targeted accounts
- Coaching - email strategy, campaigns, cadence, best practices
- Review goals at the account level and personal level
- Strategy for next week
- Upcoming events/campaigns that can be leveraged
- Personal goals and commitments


# BDR Workflow<a name="bdring"></a>
- Strategize to develop the proper qualifying questions for all types of customers.
- Be able to identify where a customer is in the sale and marketing funnel and take the appropriate action
- Generate Sales Qualified Leads (SQLs)
- Inbound to work off of leads within SFDC
- Inbound does not touch any lead that has activity on it within the last 45 days by a different BDR.

## Lead/MQL Definition<a name="mql"></a>
A [Marketing Qualified Lead (MQL)](https://docs.google.com/a/gitlab.com/document/d/1_kSxYGlIZNNyROLda7hieKsrbVtahd-RP6lU6y9gAIk/edit?usp=sharing) is a lead that reaches a certain point threshold (90 pts) based on a demographic information, engagement, and/or behavior. Each BDR will be placed into the Marketo Queue and will receive a high volume of MQLs to work. Criteria for those leads are set by Marketo and the Lead Generation team. We have two MQL buckets: (1) hand raisers and (2) passive leads.

### Hand Raisers (A Leads)<a name="aLeads"></a>
Hand raisers are leads who have filled out a sales contact us form or signed up for a free EE trial. These leads are automatic MQLs regardless of demographic information because they have exhibited clear interest in GitLab’s ability to fulfill a software need.

### Passive Leads<a name="passiveLeads"></a>
Passive leads are leads who have not filled out a contact us form or signed up for a free EE trial. For a passive lead to become an MQL, they must meet our minimum lead score of 90 pts. A passive lead becomes a MQL via a combination of demographic score and behavior/engagement score.

Because these leads can MQL without showing clear interest in GitLab paid products, they are routed to Business Development Representatives (BDRs) to qualify and assess sales-readiness.

## Lead Management<a name="leadManagement"></a>
What leads to qualify, how to do it, and who to send them to.

### Glossary<a name="glossary"></a>
AM = Account Manager  
AE = Account Executive  
RD = Regional Director  
BDR = Business development Rep  
SDR = Sales Development Rep (FKA Outbound BDR)  
CS = Customer Success (Sales Engineer)  
Sales Admin = Sales Administrator (US and EMEA)  
TEDD = Technology, Engineering, Development and Design dataset from Discover.org  

### Segmentation<a name="segmentation"></a>

#### Size
(can be found on account page in Salesforce)  
Strategic = 10,001+ users  
Large = 751-10,000 users  
Mid Market = 101-750 users  
SMB (Small Medium Business) = 1-100 users   

#### Region/Vertical

##### [APAC](https://docs.google.com/document/d/1Ar0Y49XF0pnvWjhr5-jr0MNKdxfRY2FkS4x9y7KCZw8/edit#)
Asia Pacific
RD = Michael Alessio

##### [EMEA](http://genesisworld.com/assets/uploads/2014/11/map_EMEA.jpg)
Europe, Middle East, and Africa
RD = Richard Pidgeon

##### [US East](https://dev.gitlab.org/gitlab/salesforce/issues/118)
RD = Mark Rogge

##### [US West](https://dev.gitlab.org/gitlab/salesforce/issues/118)
RD = Haydn Mackay

##### Government
Director of Federal Sales = Paul Almeida

### Lead Routing<a name="leadRouting"></a>
- If Large or Strategic from unowned account or account record does not exist in SFDC, assign and notify the RD in the correct region

- If Large or Strategic from owned account, assign to SDR for that account. If no SDR assigned, assign to the account owner

- If MidMarket current customer assign to AM. If Large or Strategic current customer assign to account owner

- If none of the above BDR is empowered to handle

- If the lead is an existing customer in SFDC - Do not qualify, pass it to the account owner

- All SMB opportunities assign to Sales Admin (Chet for NA, Conor for EMEA, Julie for APAC)

- Only work new business leads

### Lead Source<a name="leadSource"></a>  
Lead Source is set upon first "known" touch attribution. It should never be changed or overwritten. If merging leads, keep the Lead Source that was created first (if you can tell). If creating a Lead/Contact and you are unsure what Lead Source to use, ask on #Lead-Questions channel in Slack.
- Advertisement
- AE Generated
- CE Download
- Conference
- Consultancy Request
- Contact Request
- Datanyze
- Development Request
- Demo  
- DiscoverOrg
- Email Request
- Enterprise Trial
- Existing Client
- External Referral
- GitLab.com
- GitLab Hosted
- Gitorious
- Leadware
- Legacy
- LinkedIn
- Live Event
- Newsletter
- Other
- Partner
- Public Relations
- Security Newsletter
- Trade Show
- Training Request
- Web
- Webcast
- Web Direct
- White Paper
- Word Of Mouth


### Lead Status<a name="leadStatus"></a>
[Lead status definitions](https://docs.google.com/spreadsheets/d/1gCJHp7r8vzsxokDBbJiUO3FPLN9Eq_De08YxhSqEwhk/edit#gid=0) are in the process of changing/transitioning, if you have questions about current status please ask in #Lead-Questions channel on Slack.
- Raw
- Open
- Attempt 1-5
- Progressing
- Qualified
- Nurture
- Unqualified
- Bad Data
- Referred to Partner
- Web Portal Purchase

## Researching<a name="researching"></a>
### 3 X 3 X 3
Three things in three minutes about the:
- Organization
- Industry
- Prospect/Lead

### Salesforce
- Search Salesforce (SFDC) for the company name.
    - Are there other leads that are being worked?
    - Activity History
        - Who in the organization have we been in contact with? Can we follow up or reference this?
    - Is there an owned customer/prospect account?
    - Are there any open opportunities?

### LinkedIn
- Summary/bio
    - Does anything stand out that is relevant to their needs as an organization?
    - What is the lead’s role and how does that affect your messaging?
    - Have they published any articles that would be worth referencing?
- Previous work
    - Any Gitlab Customers that we can reference?
- Connections
    - Are they connected to anyone at GitLab for a possible introduction?

### Company Website
- Mission Statement
- Press Releases/Newsroom
- (ctrl F) search for keywords

## Prospecting<a name="prospecting"></a>

### Email Prospecting<a name="emailProspect"></a>
- Studies show that Executives read emails in the morning
- Expect to be forwarded to the right person (Always ask)
- Keep emails 90 words or less
- Break into readable stanzas
- Always be specific and tailored
- Make your emails viewable from a mobile device

#### Structuring a Prospecting Email
- Subject line
- Preview pain
- Opening stanza
    - Make it about them, not about you
- Benefit and value proposition
- Call to action (CTA)
    - Offer available times for a meeting/call
    - Create urgency

### Call Prospecting<a name="callProspect"></a>
- Call about them, not about you
- Be confident and passionate
- Aim for every role but focus on decision makers
- Ask for time
- Focus on your endgame (SQL)
- Make it easy to say yes
- Obtain a commitment

### Structuring a Prospecting Call
- Introduction
    - Immediately introduce yourself and GitLab
    - Ask for time
- Initial benefit statement
- Qualification
- Inform them/answer questions about GitLab
- Commitments

### Warm Discovery Calls
Purpose: To qualify leads faster and more accurately (SQL Qualification Criteria) than in an email exchange.

Process: In your reply message to setup/initiate a call, ask a few of your normal BDR questions to prep for the call. To save a step in emails include your Calendly link so leads may schedule the call directly.

    “Hi (lead name), this is (BDR) from GitLab. How are you today?”

    “Great, is now still a good time to talk about (primary issue)?”

After you’ve established the conversation is good to move forward, ask questions and guide the conversation in a way that enables the lead to tell you what their issue/problem is while also providing answers to the SQL QC. Your primary role is to gather information, and decide more appropriately how to provide assistance and/or qualify the lead for a call with an AE.

    “Great (lead name), thank you for sharing that information. I have a better feel now for how to move forward with your request/issue. I’m going to follow-up with an email recapping what we discussed, send over that documentation I promised and get you in touch with (account executive)”

## Qualifying<a name="qualifying"></a>
Your goal is to generate Sales Qualified Leads (SQLs) by gathering all pertinent information needed for an Account Executive to take action. Some examples of sales qualification questions can be found [here](https://about.gitlab.com/handbook/sales-qualification-questions/). 	

### SQL Qualification Criteria<a name="sql"></a>

#### Current Defined Need
- Does the prospect have an identified need for GitLab?
- What is the prospect currently doing to address their need? What other technologies are they using?
- What version of EE are they interested in?
- [EE Product Qualification Questions](https://about.gitlab.com/handbook/EE-Product-Qualification-Questions/)

#### Budget
- Is there a budget secured for the project?
- Does the prospect have a realistic chance of securing the budget for GitLab?

#### Buying Process
- Who is the decision maker for GitLab?
- What is the buying process for procuring GitLab?
- What role does the prospect play in the current evaluation?

#### Timeline
- What is the timeline to make a decision?
- Are you currently in an existing contract that needs to expire before you can move forward? If yes, when does the contract expire?

#### Product Fit
- Are they interested in GitLab EE? Is GitLab EE a good fit for their need? (Needs to be YES)
- Do they currently use another version of GitLab?
- Are they familiar with our product family?

#### Scope
- How many seats are they interested in purchasing?
- What version of EE are they interested in?

#### Next Steps
- Is there a meeting set with an AE to discuss next steps?
- Did they request a quote?
- Are there specific questions/issues the AE should address?

### When to Create an Opportunity<a name="opportunity"></a>
When you have successfully qualified a lead, you will need to create an opportunity and handoff the lead to the assigned Account Executive (AE)/Account Owner.

Before a lead is converted or an opportunity is created the following must occur:
- Identified problem GitLab can solve - happens during introductory call
- Interest by prospect to learn more about GitLab - moving to discovery stage
- Discovery call scheduled with prospect - moving to discovery stage
- Interest by GitLab salesperson to pursue the opportunity

#### How to Convert a Lead Into an Opportunity
[This](https://docs.google.com/document/d/1_Q7DIE1tM7IOuCyQXLPFjOBAS52AL7QadpLb7pshw48/edit) document provides a step by step process on how to convert a lead to an opportunity. It includes the following information:
- [Opportunity Naming Conventions](https://about.gitlab.com/handbook/sales/#opportunity-naming-convention)
- Required fields necessary for an opportunity be saved correctly

#### Handoff Process
Schedule a Discovery Call with the Account Owner/AE
- Schedule the call via Google Calendar, send invites
- Include yourself, the AE, and the prospect on the invite, it is required you join the call
- Name the event: Gitlab Discovery Call - {{Account Name}}
- Insert the AE’s [Zoom meeting link](https://docs.google.com/document/d/1GFpXMjOS-kBsqNV5aFdMho0c4NfU2311MkX-4cogVfQ/edit) into the ‘Where’ section of the Google Calendar invite.

# Miscellaneous<a name="miscellaneous"></a>

## Variable Compensation Guidelines<a name="compensation"></a>
Full-time BDRs have a significant portion of their pay based on performance and objective attainment. Performance based "bonuses" are based quota attainment.

Actions for obtaining results will be prescribed and measured, but are intentionally left out of bonus attainment calculations to encourage experimentation. BDRs will naturally be drawn to activities that have the highest yield, but freedom to choose their own daily activities will allow new, higher yield activities to be discovered.

### Guidelines for Bonuses
- Team and individual quotas are based on GitLab's revenue targets
- Quotas will be made known by having each BDR sign a participant form that clearly lays out quarterly quotas that match the company's revenue plan
- Bonuses are paid quarterly.
- Bonuses are based solely on Sales Qualified Pipeline generated.
- A new BDR's first month's bonus is typically based on completing onboarding

## Additional Resources<a name="additionalResources"></a>
- [GitLab Primer](https://about.gitlab.com/primer/)
- [Glossary of Terms](https://docs.gitlab.com/ce/university/glossary/README.html)
- [GitLab Positioning](https://about.gitlab.com/handbook/positioning-faq/)
- [EE Product Qualification Questions](https://about.gitlab.com/handbook/EE-Product-Qualification-Questions/)
- [Sales Qualification Questions](https://about.gitlab.com/handbook/sales-qualification-questions/)
- [FAQ from Prospects](https://about.gitlab.com/handbook/sales-faq-from-prospects/)
- [Customer Use Cases](https://about.gitlab.com/handbook/use-cases/)
- [GitLab University](https://docs.gitlab.com/ce/university/)
- [Platzi GitLab Workshop](https://courses.platzi.com/classes/git-gitlab/)
- [GitLab Market and Ecosystem](https://www.youtube.com/watch?v=sXlhgPK1NTY&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e&index=6)
- [GitLab Documentation](http://docs.gitlab.com/ee/)
- [GitLab CI/CD Demo](https://about.gitlab.com/2017/03/13/ci-cd-demo/)
- [GitLab Compared to Other Tools](https://about.gitlab.com/comparison/)
- [GitLab Battlecards](https://docs.google.com/document/d/1zRIvk4CaF3FtfLfSK2iNWsG-znlh64GNeeMwrTmia_g/edit#)
- [How to Use Outreach](https://docs.google.com/document/d/1FzvGEsL6ukxFZtk7ZkMUVAAT_wW-aVblAu1yOFWiEGo/edit)
- [Outreach University](http://university.outreach.io/)
- [Version.gitlab](https://version.gitlab.com/users/sign_in)
- [Resellers Handbook](https://about.gitlab.com/handbook/resellers/)
- [Customer Reference Sheet](https://docs.google.com/a/gitlab.com/spreadsheets/d/1Off9pVkc2krT90TyOEevmr4ZtTEmutMj8dLgCnIbhRs/edit?usp=sharing)
- [Support Handbook](https://about.gitlab.com/handbook/support/)
- [GitHost](https://about.gitlab.com/gitlab-hosted/)
- [Additional Training Resources](https://drive.google.com/drive/folders/0B1W9hTDXggO1NGJwMS12R09lakE)

## FAQ<a name="faq"></a>
Don't hesitate to ping one of your colleagues with a question, or someone on the team who specializes in what you're searching for. Everyone is happy to help, and it's better to get your work done faster with your team, than being held up at a road block.

### Questions specific to:

#### Salesforce
- #sfdc-users
- Francis Aquino

#### Lead Questions or Issues
- #lead-questions
- JJ Cordz

#### GitLab
- #support
- Assigned buddy
- BDR Team Lead (Chet Backman)

#### Outreach
- #outreach
- Chet Backman
- Francis Aquino

#### Resellers
- Michael Alessio

#### Partnerships
- Eliran Mesika

### What are my work hours?
The handbook says “You should have clear objectives and the freedom to work on them as you see fit. Any instructions are open to discussion. You don’t have to defend how you spend your day. We trust team members to do the right thing instead of having rigid rules”.

### Who should I connect with for my 10 1:1 onboarding calls?
You can connect with whomever you please. Check out the [Team Page](https://about.gitlab.com/team/) for ideas. Though you will be working most closely with the Sales and Marketing teams, it is encouraged you get to know people all around the organization.

### Improvements/Contributing<a name="contribute"></a>
- If you get an answer to a question someone else may benefit from, incorporate it into this handbook or add it to the FAQ document
- After meetings or process changes, feel free to update this handbook, and submit a merge request.
- Create issues for any idea (small or large that), that you want feedback on
- All issued and MRs for changes to the BDR handbook assign to Chet Backman

---

# Outbound SDR Handbook

## Job Description

As a Sales Development Representative (SDR) you are focused on driving net new business for the company. We thrive on competition and our ability to cold call/email. You will be aligned with Account Executives (AE’s) to work a strategic list of 50-100 targeted accounts on a quarterly basis..

## Expectations

You will be expected to:
* Build a trusting relationship with your assigned AE’s
* Hit your monthly goals. Which are opportunities/SQLs (Sales Qualified Leads) generated
* Have a good balance of phone calls made and emails sent (there are no daily metric requirements around this)
* Complete the GitLab beginner, intermediate, and expert certification courses
* Be able to demo GitLab to one of the sales directors

## SDR Onboarding

To prepare you to be a successful SDR at GitLab we have created a 4 week bootcamp (link to sales bootcamp url). At the end of 4 weeks, success is defined as preparing you to have conversations with prospects and create sales opportunities.
Each week there will be a defined goal of what you should learn and be able to apply with information and training to support the learning objective.

### [SDR Master Onboarding](https://docs.google.com/document/d/1ieddQChPZplU7toak2mZsyB3MiXQkbiwkJ24zS3Fjxo/edit)
### [SDR Playbook Onboarding](https://about.gitlab.com/handbook/sales/sdr/sdr-playbook-onboarding/)

## Working with AE’s

A good portions  of your job requires you to work with assigned AE’s. This is key to our role as SDRs’ because they will not only help you create an appropriate strategy to go after specific accounts, but they will help you build your career as a future AE for GitLab. It is crucial that you build a relationship of honesty and trust.

### Account Distribution

* When you are assigned to your AE’s you will work with them to create a list of accounts for you to go after. This should consist of ~50-100 Large or Strategic accounts to work in a given quarter.
* In the situation that you do not have assigned AE’s, your Team Lead will provide you with a list of unnamed top strategic or large accounts to go after. (When you create meetings you will need to coordinate with your Team Lead to decide what AE that account will go to. You will also work with your Team Lead to develop strategy for your assigned accounts).
* In the event that you feel your accounts are non-workable please consult with your Team Lead.
* We use this [report](https://na34.salesforce.com/00O61000003iZLP) to track and ensure proper coverage of stategic and large accounts and effective use of SDR resources.  

### Account Management

* We use SalesForce as our CRM to manage accounts.
* When it comes to task management there are a couple of different ways to do so. You can use either SFDC (SalesForce.com) or you can use Outreach which is primarily used for emailing.

### SDR/AE Relationship

* Communication is priority in our company. You will need to create a working relationship with your assigned AE’s.
* The relationship between you and your AE’s should be one of trust, honesty, and openness. It is important to understand that this relationship is a two-way street. The SDR and the AE will need to put in equal amounts of effort to make this relationship work. When the relationship works, both the SDR and AE become successful.
* There should be, at minimum, a weekly 1:1 with each your AE’s to discuss:
* Where you are at for the month, metric wise.
* What you have done with the accounts you are working. (This will be what is working well, what you have found out about company structure, who are the right people to talk to, and meetings/opportunities you have created.)
* Discuss evolving strategy for “currently working” accounts and “new accounts” you will be going after.

## Metrics

* SDR’s compensation is based on 1 key metrics:
   * Opportunities created (SQL’s)

* SDRs’ will also be measured on, but not tied to their compensation:
    * Number of contacts added to named account
    * Account Mapping
    * Roles identified for each contact
    * Software development tools being used
    * How the organization is set up - who are the other teams that we can expand into
    * Purchasing process - do they have a procurement team, do/can they use a fulfillment agency
    * % of named accounts contacted
    * Number of contacts with activity (email and/or calls)
    * Number of emails sent
    * Number of calls made
    * Number of connects (calls between SDR and prospect)
    * Number of sales appointments (calls between prospect, AE and SDR to explore if there is an opportunity)
    * Pipeline of SDR sourced opportunities
    * ACV won from opportunities SDR sources

## SDR Daily Stand-up Meeting

The purpose of the SDR daily stand-up meeting is to set daily goals for ourselves and to review what we did the day before. This helps each of us individually to make sure we are on track to hit our monthly numbers. Also, it is a good time for us to reflect on what is working well and what we are struggling with when it comes to our goals we set each day.

## SDR Weekly Team Meeting

The purpose of the weekly SDR team meeting is to go over important changes that are happening with the team, update on monthly metrics, open discussion on any questions people have, hiring updates, etc. It is important for us to meet each week as a team just to see each other and to be able to talk with each other as a group.

## Criteria for an SQL

 1. Authority
  * What role does this person play in the decision process? (i.e. decision maker, influencer, etc.)
  * OR is this someone that has a pain and a clear path to the decision maker

 2. Need
  * The buyer has either a defined project or a pain has been identified
  * If organization already has an open opportunity or is a customer, SQL needs to be for a different group, buying process and budget.

 3. Required Information
  * Number of potential EE users on this opp
  * Current technologies and when the contract is up
  * Current business initiatives
  * When naming opportunity, make sure to add in division or group to the opportunity name

 4. Handoff
  * The buyer is willing to meet with an AE

## Criteria for Sales Accepted Opportunity (SAO)

 1. SQL Definition Met
  * AE confirmed authority and need
  * AE confirms number of potential EE users
  * AE confirms that the SQL information is logged into SFDC

![alt text](source/images/Lead_Qual_Questions.png)

 2. Handoff
  * The SDR has made an introduction with the AE to the prospect via email
  * SDR schedules a calendar invite with buyer and AE
  * The SDR confirms the agenda and set expections for the phone call

 3. Meeting Completed
  * The AE conducts the first meeting via phone (it is recommended that the SDR is on the phone call as well)
  * The AE re-confirms the information passed by the SDR

 4. Next Step Identified
  * The AE schedules a next event with the AE (typically another phone call to dive in deeper to the clients pains and needs)
  * The AE has 1 business day to accept/reject the opportunity

## Ramping for new hires

**If you started carrying a quota at the beginning of the month:**

Month 1:

 * Daily/Weekly/Monthly Activity [40/150/600]
 * Daily/Weekly/Monthly Emails [20/100/400]
 * Daily/Weekly/Monthly Calls [10/50/200]
 * % of named accounts contacted (monthly) [25%]
 * Average # of contacts per named account contacted (monthly) [5]
 * Decision Makers confirmed/added (weekly) - persons they identify in their research and calls not from discover.org.  Who makes the decision for software development tools (i.e. purchased GitHub, Jenkins, JIRA) [4]
 * Connects (weekly/monthly) - calls SDR’s are having [5/15]
 * Sales Appointments (weekly/monthly) - calls SDR’s and AE’s are having with prospect (influencer or Decision Maker) [8]
 * % of named accounts with sales appointment [10%]
 * Opportunities created (weekly/monthly) [0.75/3]
 * * % of named accounts with an opportunity [10%]
 * ACV won from sourced opportunities (monthly/quarterly) [$0]

Month 2:

 * Daily/Weekly/Monthly Activity [60/300/1200]
 * Daily/Weekly/Monthly Emails [40/200/800]
 * Daily/Weekly/Monthly Calls [20/100/400]
 * % of named accounts contacted (monthly) [50%]
 * Average # of contacts per named account contacted (monthly) [10]
 * Decision Makers confirmed/added (weekly) - persons they identify in their research and calls not from discover.org.  Who makes the decision for software development tools (i.e. purchased GitHub, Jenkins, JIRA) [3]
 * Connects (weekly/monthly) - calls SDR’s are having [10/40]
 * Sales Appointments (weekly/monthly) - calls SDR’s and AE’s are having with prospect (influencer or Decision Maker) [3/12]
 * % of named accounts with sales appointment [33%]
 * Opportunities created (weekly/monthly) [1.25/5]
 * % of named accounts with an opportunity [33%]
 * ACV won from sourced opportunities (monthly/quarterly) [$0]

Month 3+:

 * Daily/Weekly/Monthly Activity [90/450/1800]
 * Daily/Weekly/Monthly Emails [60/300/1200]
 * Daily/Weekly/Monthly Calls [30/150/600]
 * % of named accounts contacted (monthly) [75%]
 * Average # of contacts per named account contacted (monthly) [15]
 * Decision Makers confirmed/added (weekly) - persons they identify in their research and calls not from discover.org.  Who makes the decision for software development tools (i.e. purchased GitHub, Jenkins, JIRA) [4]
 * Connects (weekly/monthly) - calls SDR’s are having [15/60]
 * Sales Appointments (weekly/monthly) - calls SDR’s and AE’s are having with prospect (influencer or Decision Maker) [5/20]
 * % of named accounts with sales appointment [33%]
 * Opportunities created (weekly/monthly) [2/8]
 * % of named accounts with an opportunity [33%]
 * ACV won from sourced opportunities (monthly) [$80,000]

**If you started carrying a quota halfway through the month:**

Month 1:

 * Daily/Weekly/Monthly Activity [40/150/300]
 * Daily/Weekly/Monthly Emails [20/100/400]
 * Daily/Weekly/Monthly Calls [10/50/200]
 * % of named accounts contacted (monthly) [25%]
 * Average # of contacts per named account contacted (monthly) [5]
 * Decision Makers confirmed/added (weekly) - persons they identify in their research and calls not from discover.org.  Who makes the decision for software development tools (i.e. purchased GitHub, Jenkins, JIRA) [4]
 * Connects (weekly/monthly) - calls SDR’s are having [5/15]
 * Sales Appointments (weekly/monthly) - calls SDR’s and AE’s are having with prospect (influencer or Decision Maker) [6]
 * % of named accounts with sales appointment [10%]
 * Opportunities created (weekly/monthly) [1/2]
 * * % of named accounts with an opportunity [10%]
 * ACV won from sourced opportunities (monthly/quarterly) [$0]

Month 2:

 * Daily/Weekly/Monthly Activity [60/300/1200]
 * Daily/Weekly/Monthly Emails [40/200/800]
 * Daily/Weekly/Monthly Calls [20/100/400]
 * % of named accounts contacted (monthly) [50%]
 * Average # of contacts per named account contacted (monthly) [10]
 * Decision Makers confirmed/added (weekly) - persons they identify in their research and calls not from discover.org.  Who makes the decision for software development tools (i.e. purchased GitHub, Jenkins, JIRA) [3]
 * Connects (weekly/monthly) - calls SDR’s are having [10/40]
 * Sales Appointments (weekly/monthly) - calls SDR’s and AE’s are having with prospect (influencer or Decision Maker) [3/12]
 * % of named accounts with sales appointment [33%]
 * Opportunities created (weekly/monthly) [1.25/5]
 * % of named accounts with an opportunity [33%]
 * ACV won from sourced opportunities (monthly/quarterly) [$0]

Month 3+:

 * Daily/Weekly/Monthly Activity [90/450/1800]
 * Daily/Weekly/Monthly Emails [60/300/1200]
 * Daily/Weekly/Monthly Calls [30/150/600]
 * % of named accounts contacted (monthly) [75%]
 * Average # of contacts per named account contacted (monthly) [15]
 * Decision Makers confirmed/added (weekly) - persons they identify in their research and calls not from discover.org.  Who makes the decision for software development tools (i.e. purchased GitHub, Jenkins, JIRA) [4]
 * Connects (weekly/monthly) - calls SDR’s are having [15/60]
 * Sales Appointments (weekly/monthly) - calls SDR’s and AE’s are having with prospect (influencer or Decision Maker) [5/20]
 * % of named accounts with sales appointment [33%]
 * Opportunities created (weekly/monthly) [2/8]
 * % of named accounts with an opportunity [33%]
 * ACV won from sourced opportunities (monthly) [$80,000]

### Salesforce Hygiene for your opportunites

[SDR sourced opps](https://na34.salesforce.com/00O61000003iXGw) This is the report that the leadership uses to see all the opps that have been created from the SDR team. I strongly recommend that you live in this report for the opps that you are creating. If you make sure that all the above information is as accurate as possible it will help you to not be under the microscope from your manager and the executive team.

I highly recommend also to stay on top of your AE’s to make sure they are progressing the opportunity from BDR qualified to Discovery stage to make sure you get paid on the opps that you are creating. All of the above steps and processes are only going to help you create healthy habits for when you become an AE.

It will be in your best interest to also sit in on as many meetings as possible with your different AE’s and at different stages in the buying process to see how the AE’s work with prospects beyond qualifying. The idea is to create a habits for success.
