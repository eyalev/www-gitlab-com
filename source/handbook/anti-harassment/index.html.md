---
layout: markdown_page
title: "Anti-Harassment Policy"
---


## Introduction

Everyone at GitLab has a responsibility to prevent and stop harassment. Working remotely means that the majority of our interactions are by video call and written communication such as email or shared documents, with exception of team summits, attending conferences together and local team meetups. No matter what the method of communication, it is expected that everyone will contribute to an inclusive and collaborative working environment and respect each other at all times. Should you become aware or witness any form of harassment or behavior that violates this policy or our [company values](https://about.gitlab.com/handbook/values/), please report the incident directly to the VP of People Operations or People Operations Generalist immediately for thorough investigation.

## Scope

This policy applies to all team members employed by any entity of GitLab, whether contractor or employee, in all locations (this policy will be regularly updated to ensure compliance with legislation in each country that GitLab has an entity or co-employer relationship). Country specific regulations that differ from this policy can be found: TODO. All individual contributors, managers, and leaders will be subject to disciplinary action, up to and including termination, for any act of harassment they commit.

Types of Harassment

The following are considered forms of harassment and will not be tolerated by GitLab:
- Sexual Harassment
- Discrimination
- Bullying / Workplace Violence

## Sexual Harassment

Sexual harassment is considered unwelcome conduct of a sexual nature that is sufficiently persistent or offensive enough to interfere with the receiver’s job performance or create an intimidating, hostile or offensive working environment.

Sexual harassment encompasses a wide range of conduct, examples of misconduct include, but may not be limited to, the following actions:

1. Physical assaults or the attempt to commit an assault of a sexual nature. This physical conduct can include touching, pinching, patting, grabbing, brushing against or poking another team member's body.
1. Unwelcome sexual advances, propositions or other sexual comments, such as sexually oriented gestures, noises, remarks, jokes or comments about a person’s sexuality or sexual experience.
1. Preferential treatment or promises of preferential treatment to a team member for submitting to sexual conduct, including soliciting or attempting to solicit any team member to engage in sexual activity for compensation or reward.
1. Subjecting, or threats of subjecting, a team member to unwelcome sexual attention or conduct or intentionally making performance of the team member's role more difficult because of that team member's sex.
1. Creating displays, communications, or publications including content of a sexually offensive nature.
1. Purposely misgendering people, consistently referring to someone as 'he' after repeated requests to call someone a 'she' or vice versa.


## Discrimination

Any form of discrimination towards an individual is strictly prohibited. Types of discrimination may include:

- Age
- Disability
- Race; including color, nationality, ethnic or national origin
- Religious belief or lack of religious belief
- Life expectancy
- Sex
- Sexual orientation
- Transgender status

If you believe you have been discriminated against or witnessed discriminatory practices, please contact the VP of People Operations or the People Operations Generalist to initiate an investigation into the behavior.

## Bullying / Workplace Violence

GitLab does not tolerate violent acts or threats of violence. The company will not tolerate fighting, bullying, coercion or use of abusive or threatening words directed to, about or against a co-worker, lead, manager, executive, applicant, client/customer or vendor. No individual employed by GitLab should commit or threaten to commit any violent act or discuss committing such offenses, even in a joking manner.

## Reporting Alleged Harassment

1. Any individual who believes they have been the target of harassment of any kind is encouraged to immediately and directly address the harasser, letting them know that their behavior is unwelcome, offensive and must stop immediately.
1. If they do not wish to address the harasser directly or the behavior doesn’t cease, they should report the misconduct to the VP of People Operations or the People Operations Generalist.
1. Once reported, an impartial investigation will be conducted by People Operations or by an independent third party, depending on the severity and circumstances of the complaint.
1. Individual(s) reporting an incident or pattern of behavior will be asked to provide a written account of any action(s) causing concern, dates and times such actions occurred, and names of anyone involved, including participants and witnesses. All complaints or concerns of alleged harassment or discrimination will be taken seriously and handled confidentially.

## The Role of Managers

If managers become aware of misconduct, they must deal expeditiously, seriously, confidentially and fairly with any allegations, whether or not there has been a written or formal complaint made to People Operations. Informed managers are expected to:

1. Take all complaints or concerns of alleged harassment seriously no matter how minor or who is involved.
1. Ensure that any form of harassment or misconduct is immediately reported to People Operations.
1. Take appropriate action to prevent retaliation or the alleged misconduct from recurring during and after an investigation.

Managers who knowingly allow or tolerate any form of harassment or retaliation, including the failure to immediately report such misconduct to People Operations, are in violation of this policy and subject to disciplinary action, including termination.

## The Role of People Operations

The VP of People Operations and People Operations Generalist are responsible for:

1. Ensuring that any individual filing a complaint and any accused individual(s) is made aware of the seriousness of misconduct.
1. Explaining GitLab's no tolerance harassment policy and investigation procedures to all individuals included in a complaint.
1. Arranging for an immediate investigation of alleged misconduct and the preparation of a written report summarizing the results of the investigation and making recommendations for remediation to designated company officials.
1. Notifying appropriate authorities (police, FBI, country specific bureaus) when criminal activities are alleged.
1. Exploring informal means of resolving potential harassment if a written (formal) complaint is not made when verbal allegations are shared.

## Training & Guidance

Training and guidance on understanding, preventing and dealing with discrimination & sexual harassment will be assigned to everyone through the learning management system, Grovo. Completion of this training will be required within two weeks of assignment. Those not in compliance will be subject to disciplinary action. Training will be required annually.

## Retaliation

Retaliation of any sort for filing a claim of harassment will not be tolerated.  If you believe you have been retaliated against, please contact the VP of People Operations or the People Operations Generalist to initiate an investigation.

## Resources

We are continuously gathering country specific references to review regulation and obtain guidance on the management of harassment or misconduct at work. Here are a few authorities we referred to in the creation of this policy:

- [Equal Employer Opportunity Commission (EEOC)](https://www.eeoc.gov/laws/index.cfm)
- [Society of Human Resource Management (SHRM)](https://www.shrm.org/ResourcesAndTools/legal-and-compliance/employment-law/pages/federal-statutes-regulations-and-guidance.aspx)

## Further Guidance (Country Specific)

- [UK: Acas (advisory service for employees and employers)](http://www.acas.org.uk/index.aspx?articleid=1864)
- [The Netherlands: Inspectorate SZW](https://www.inspectieszw.nl/)
- [India: Ministry of Labour & Employment](http://www.labour.nic.in/)
- [Belgium: Unia: For equality, against discrimination](http://unia.be/en)
